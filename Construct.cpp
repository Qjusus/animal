﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "\nhey , bro ";

    }

};

class Dog : public  Animal
{
public:
    void Voice() override
    {
        std::cout << "\nWoof!";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "\nMeow";
    }
};

class Goose : public Animal
{
public:
    void Voice() override
    {
        std::cout << "\nGaaa";
    }
};


int main()
{
   /*
   Animal* p = new Animal;
    Animal* p1 = new Dog;
    Animal* p2 = new Cat;
    Animal* p3 = new Goose;

    p->Voice();
    p1->Voice();
    p2->Voice();
    p3->Voice();
    */

    Dog* dog = new Dog;
    Cat* cat = new Cat;
    Goose* goose = new Goose;
    Animal** animallist = new Animal * [3] { dog, cat, goose };

    for (int i = 0; i < 3; i++)
    {
        animallist[i]->Voice();
    }
        
}